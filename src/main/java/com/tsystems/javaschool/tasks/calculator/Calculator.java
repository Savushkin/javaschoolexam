package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement)
    {
        try {
            Result result = plusMinus(statement);
            if (!result.getRest().isEmpty()) {
                return null;    //can't parse rest part of string
            }

            //format result to #.####
            DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols();
            otherSymbols.setDecimalSeparator('.');
            DecimalFormat df = new DecimalFormat("#.####", otherSymbols);
            return df.format(result.getTotal());
        } catch(Exception ex) {
            return null;
        }

    }

    private Result plusMinus(String statement) throws Exception
    {
        Result currentResult = multiplyDivision(statement);
        if (currentResult == null) {
            throw new Exception("Error: multiplyDivision return null");
        }
        double total = currentResult.getTotal();

        while (currentResult.getRest().length() > 0) {
            if (!(currentResult.getRest().charAt(0) == '+' || currentResult.getRest().charAt(0) == '-')) break;

            char sign = currentResult.getRest().charAt(0);
            String nextPartOfStatement = currentResult.getRest().substring(1);

            currentResult = multiplyDivision(nextPartOfStatement);
            if (currentResult == null) {
                throw new Exception("Error: multiplyDivision return null");
            }
            if (sign == '+') {
                total += currentResult.getTotal();
            } else {
                total -= currentResult.getTotal();
            }
        }

        currentResult.setTotal(total);
        currentResult.setRest(currentResult.getRest());
        return currentResult;
    }

    private Result Bracket(String statement) throws Exception
    {
        char zeroChar = statement.charAt(0);
        if (zeroChar == '(') {
            Result resultOfPlusMinus = plusMinus(statement.substring(1));
            if (!resultOfPlusMinus.getRest().isEmpty() && resultOfPlusMinus.getRest().charAt(0) == ')') {
                resultOfPlusMinus.setRest(resultOfPlusMinus.getRest().substring(1));
            } else {
                throw new Exception("Error: don't close bracket");
            }
            return resultOfPlusMinus;
        }
        return getNumberFromString(statement);
    }

    private Result multiplyDivision(String statement) throws Exception
    {
        Result bracketResult = Bracket(statement);

        double total = bracketResult.getTotal();
        while (true) {
            if (bracketResult.getRest().length() == 0) {
                return bracketResult;
            }
            char sign = bracketResult.getRest().charAt(0);
            if ((sign != '*' && sign != '/')) return bracketResult;

            String nextPartOfStatement = bracketResult.getRest().substring(1);
            Result rightPartOfStatement = Bracket(nextPartOfStatement);

            if (sign == '*') {
                total *= rightPartOfStatement.getTotal();
            } else {
                if (rightPartOfStatement.getTotal() == 0) {
                    return null;
                }
                total /= rightPartOfStatement.getTotal();
            }

            bracketResult.setTotal(total);
            bracketResult.setRest(rightPartOfStatement.getRest());
        }
    }

    private Result getNumberFromString(String str) throws Exception
    {
        int i = 0;
        int dot_cnt = 0;

        //check digits and dot
        while (i < str.length() && (Character.isDigit(str.charAt(i)) || str.charAt(i) == '.')) {
            //check double dots
            if (str.charAt(i) == '.' && ++dot_cnt > 1) {
                throw new Exception("not valid number '" + str.substring(0, i + 1) + "'");
            }
            i++;
        }

        //not found valid number
        if( i == 0 ){
            throw new Exception( "can't get valid number in '" + str + "'" );
        }

        double numberPart = Double.parseDouble(str.substring(0, i));
        String restPart = str.substring(i);

        return new Result(numberPart, restPart);
    }

    class Result
    {
        private double total;
        private String rest;

        public Result(double v, String r)
        {
            this.total = v;
            this.rest = r;
        }

        public double getTotal() {
            return total;
        }

        public void setTotal(double total) {
            this.total = total;
        }

        public String getRest() {
            return rest;
        }

        public void setRest(String rest) {
            this.rest = rest;
        }
    }

}
