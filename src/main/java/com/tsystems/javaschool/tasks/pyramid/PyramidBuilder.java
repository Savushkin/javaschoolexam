package com.tsystems.javaschool.tasks.pyramid;

import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers.contains(null) || inputNumbers.isEmpty()) {
            throw new CannotBuildPyramidException();
        }

        int heightPyramid = 0;
        /*
         * the sum of components part for checking;
         * used to test the possibility of building a pyramid
         */
        int sum = 0;
        int sizeOfInputNumbersList = inputNumbers.size();

        if (sizeOfInputNumbersList == 1) {
            throw new CannotBuildPyramidException();
        }

        while(sum < sizeOfInputNumbersList) {
            if (sum < 0 || heightPyramid < 0) {
                throw new CannotBuildPyramidException();
            }
            sum += ++heightPyramid;
        }

        if (sum > sizeOfInputNumbersList) {
            throw new CannotBuildPyramidException();
        }

        Collections.sort(inputNumbers);

        int widthPyramid = heightPyramid * 2 - 1;
        int startPosition = heightPyramid - 1;

        int[][] pyramid = new int[heightPyramid][widthPyramid];

        //building 2d pyramid
        Iterator<Integer> numbersIterator = inputNumbers.iterator();
        for(int h = 0; h < heightPyramid; h++) {
            Arrays.fill(pyramid[h], 0);
            int counterElementsForUpdate = h + 1;
            int position = startPosition;
            for(int w = 0; w < widthPyramid; w++) {
                if (w == position) {
                    pyramid[h][w] = numbersIterator.next();
                    position += 2;
                    counterElementsForUpdate--;
                }
                if (counterElementsForUpdate == 0) {
                    break;
                }
            }
            startPosition--;
        }

        return pyramid;
    }


}
