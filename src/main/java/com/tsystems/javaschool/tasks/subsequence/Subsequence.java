package com.tsystems.javaschool.tasks.subsequence;

import java.util.Iterator;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null) {
            throw new IllegalArgumentException("One of the list is null.");
        }

        if (x.isEmpty() && y.isEmpty()) {
            return true;
        }

        Iterator<?> iteratorX = x.iterator();
        Iterator<?> iteratorY = y.iterator();
        boolean resultFlag = false;
        boolean nextXFlag = true;
        Object objX = null;
        while(iteratorY.hasNext()) {
            if (nextXFlag && iteratorX.hasNext()) {
                objX = iteratorX.next();
                nextXFlag = false;
            } else if(!iteratorX.hasNext()) {
                resultFlag = true;
            }
            if (iteratorY.next().equals(objX)) {
                nextXFlag = true;
            }
        }

        return resultFlag;
    }
}
